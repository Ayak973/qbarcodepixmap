#include "../headers/mainwindow.h"
#include "ui_mainwindow.h"

#include "../headers/code128.h"

#include <QTimer>

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow) {
    ui->setupUi(this);

    //QTimer::singleShot(0, [=]() { QGuiApplication::exit(0); });
}

MainWindow::~MainWindow() {
    delete ui;
}
