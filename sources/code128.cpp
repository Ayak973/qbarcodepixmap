#include "../headers/code128.h"

#include <cmath>
#include <string>
#include <algorithm>
#include <sstream>

#include <QPainter>
#include <QResizeEvent>
#include <QDebug>


Code128::BarcodePixmapTest::BarcodePixmapTest(std::string code, int width, int height) : QPixmap(width, height), _bgColor(QColor(255, 255, 255)), _SetFrom{}, _SetTo{} {
    this->fill(_bgColor);

    for (unsigned int i = 32; i <= 95; ++i) {
        this->_ABCSet += static_cast<uint8_t>(i);
    }

    this->_ASet = this->_ABCSet;
    this->_BSet = this->_ABCSet;

    for (unsigned int i = 0; i <= 31; ++i) {
        this->_ABCSet += static_cast<uint8_t>(i);
        this->_ASet += static_cast<uint8_t>(i);
    }

    for (unsigned int i = 96; i <= 127; ++i) {
        this->_ABCSet += static_cast<uint8_t>(i);
        this->_BSet += static_cast<uint8_t>(i);
    }

    for (unsigned int i = 200; i <= 210; ++i) {
        this->_ABCSet += static_cast<uint8_t>(i);
        this->_ASet += static_cast<uint8_t>(i);
        this->_BSet += static_cast<uint8_t>(i);
    }

    this->_CSet = "0123456789";
    this->_CSet += static_cast<uint8_t>(206);

    for (int i = 0; i < 96; ++i) {
        this->_SetFrom['A'] += static_cast<uint8_t>(i);
        this->_SetFrom['B'] += static_cast<uint8_t>(i + 32);
        this->_SetTo['A'] += static_cast<uint8_t>(( (i < 32) ? (i+64) : (i-32) ));
        this->_SetTo['B'] += static_cast<uint8_t>(i);
    }

    for (int i = 96; i < 107; ++i) {
        this->_SetFrom['A'] += static_cast<uint8_t>(i + 104);
        this->_SetFrom['B'] += static_cast<uint8_t>(i + 104);
        this->_SetTo['A'] += static_cast<uint8_t>(i);
        this->_SetTo['B'] += static_cast<uint8_t>(i);
    }

//    std::stringstream sstream;
//    for (int i = 0; i < _ABCSet.length(); )
    //printString(_ABCSet);
    //printArray(_SetFrom);

//    printMapString(_SetFrom);
//    printMapString(_SetTo);

    drawCode(0, 0, code, width, height);
    qDebug() << "finished";
}

void Code128::BarcodePixmapTest::drawCode(int x, int y, std::string code, int width, int height) {
    jeu = Jeu::None;

    std::string AGuid = "";
    std::string BGuid = "";
    std::string CGuid = "";
    std::string needle;

    for (std::size_t i = 0; i < code.length(); ++i) {
        needle = code.substr(i, 1);
        AGuid += (_ASet.find(needle) == std::string::npos) ? "N" : "O";
        BGuid += (_BSet.find(needle) == std::string::npos) ? "N" : "O";
        CGuid += (_CSet.find(needle) == std::string::npos) ? "N" : "O";
    }

//qDebug() << QString::fromStdString(AGuid) << "--" <<QString::fromStdString(BGuid)<<"--"<<QString::fromStdString(CGuid);

    std::string     SMiniC  = "OOOO";
    std::size_t     IMiniC  = 4;
    std::string crypt = "";
    std::size_t  made = 0;

    while (code.length() > 0) {
        std::size_t i = CGuid.find(SMiniC);


        if (i != std::string::npos) {
            AGuid[i] = 'N';
            BGuid[i] = 'N';
        }

        if (CGuid.substr(0, IMiniC) == SMiniC) {
            crypt += (crypt.length() > 0) ? this->_JSwap['C'] : this->_JStart['C'];
            //qDebug() << QString::fromStdString(crypt);
            //printString(crypt);
            made = CGuid.find('N');

            if (made == std::string::npos) {
                made = CGuid.length();
            }

            if (std::fmod(made, 2.0f) == 1.0) {
                --made;
            }

            for (std::size_t i = 0; i < made; i += 2) {
                crypt += std::atoi(code.substr(i, 2).c_str());
            }
            jeu = Jeu::C;
        }
        else {
            std::size_t madeA = AGuid.find("N");
            if (madeA == std::string::npos) madeA = AGuid.length();

            std::size_t madeB = BGuid.find("N");
            if (madeB == std::string::npos) madeB = BGuid.length();

            made = (madeA < madeB) ? madeB : madeA;
            jeu = (madeA < madeB) ? Jeu::B : Jeu::A;

            crypt += (crypt.length() > 0) ? this->_JSwap[jeuStr(jeu)] : this->_JStart[jeuStr(jeu)];

            std::string substr = code.substr(0, made);
            char jeuChar = jeuStr(jeu);

            for (std::size_t k = 0; k < this->_SetFrom[jeuChar].length(); ++k) {
                std::replace(substr.begin(), substr.end(), this->_SetFrom[jeuChar][k], this->_SetTo[jeuChar][k]);
            }

            crypt += substr;
        }
        code = code.substr(made);
        AGuid = AGuid.substr(made);
        BGuid = BGuid.substr(made);
        CGuid = CGuid.substr(made);
    }

    if (crypt.length() == 0) throw std::logic_error("ouch");
    std::size_t check = static_cast<uint8_t>(crypt[0]);

    for (std::size_t i = 0; i < crypt.length(); ++i) {
        check += static_cast<uint8_t>(crypt[i]) * i;
    }
    check %= 103;

    //qDebug() << QString::number(check);

    crypt += static_cast<char>(check);
    crypt += static_cast<char>(106);
    crypt += static_cast<char>(107);

    std::size_t len = crypt.length() * 11 - 8;
    double module = static_cast<double>(width) / static_cast<double>(len);
qDebug() << "len: " << len << " - w: " << width << " - module: " << module;
    printString(crypt);

    QPainter painter(this);

    double xFloat = x;

    for (std::size_t i = 0; i < crypt.length(); ++i) {
        //qDebug() << (int)crypt[i];
        std::vector<int> array = T128.at(static_cast<std::size_t>(crypt[i]));
        qDebug() << array;
        for (std::size_t j = 0; j < array.size(); ++j) {
            painter.fillRect(QRectF(xFloat, y, static_cast<double>(array[j]) * module, height), QColor(0, 0, 0));
            xFloat += (array[j+1]+array[j]) * module;
            j++;
            qDebug() << xFloat;
            //$x += ($c[$j++]+$c[$j])*$modul;
        }
    }

}

void Code128::BarcodePixmapTest::printString(const std::string &str) {
    std::stringstream sstream;
    for (std::size_t i = 0; i < str.length(); ++i) {
        sstream << (int)(uint8_t)+str[i] << ":";
    }
    qDebug() << QString::fromStdString(sstream.str());
}

void Code128::BarcodePixmapTest::printMapString(const std::map<uint8_t, std::string> &map) {
    std::stringstream sstream;
    for (auto & it : map) {
        for (std::size_t i = 0; i < it.second.length(); ++i) {
            sstream << (int)(uint8_t)+it.second[i] << ":";
        }
    }
    qDebug() << QString::fromStdString(sstream.str());
}


//void Code128::BarcodePixmapTest::printArray(const std::array<Code128::BarcodePixmapTest::T, Code128::BarcodePixmapTest::N> &array) {
//    std::stringstream sstream;
//    for (int i = 0; i < array.size(); ++i) {
//        sstream << (int)(uint8_t)+array[i] << ":";
//    }
//    qDebug() << QString::fromStdString(sstream.str());
//}

char Code128::jeuStr(Jeu jeu) {
    if (jeu == Jeu::A) return 'A';
    if (jeu == Jeu::B) return 'B';
    if (jeu == Jeu::C) return 'C';
    return ' ';
}



////////////BARCODELABEL

Code128::BarcodeLabel::BarcodeLabel(QWidget *parent, Qt::WindowFlags f) : QLabel(parent, f), _pixmap(nullptr) { }

Code128::BarcodeLabel::BarcodeLabel(const QString &text, QWidget *parent, Qt::WindowFlags f) : QLabel(text, parent, f), _pixmap(nullptr) { }

void Code128::BarcodeLabel::resizeEvent(QResizeEvent *event) {
    delete _pixmap;
    _pixmap = new BarcodePixmapTest("0040000000000634690EE3914AAH", event->size().width(), event->size().height());
    setPixmap(*_pixmap);
    QLabel::resizeEvent(event);
}




