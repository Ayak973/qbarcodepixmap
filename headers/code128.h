#ifndef CODE128_H
#define CODE128_H

#include <vector>
#include <array>
#include <sstream>

#include <QPixmap>
#include <QLabel>

#include <QDebug>


namespace Code128 {

    static const std::vector<std::vector<int>> T128 = {
        { 2, 1, 2, 2, 2, 2 },           //0 : [ ]               // composition des caractères
        { 2, 2, 2, 1, 2, 2 },           //1 : [!]
        { 2, 2, 2, 2, 2, 1 },           //2 : ["]
        { 1, 2, 1, 2, 2, 3 },           //3 : [#]
        { 1, 2, 1, 3, 2, 2 },           //4 : [$]
        { 1, 3, 1, 2, 2, 2 },           //5 : [%]
        { 1, 2, 2, 2, 1, 3 },           //6 : [&]
        { 1, 2, 2, 3, 1, 2 },           //7 : [']
        { 1, 3, 2, 2, 1, 2 },           //8 : [(]
        { 2, 2, 1, 2, 1, 3 },           //9 : [)]
        { 2, 2, 1, 3, 1, 2 },           //10 : [*]
        { 2, 3, 1, 2, 1, 2 },           //11 : [+]
        { 1, 1, 2, 2, 3, 2 },           //12 : [,]
        { 1, 2, 2, 1, 3, 2 },           //13 : [-]
        { 1, 2, 2, 2, 3, 1 },           //14 : [.]
        { 1, 1, 3, 2, 2, 2 },           //15 : [/]
        { 1, 2, 3, 1, 2, 2 },           //16 : [0]
        { 1, 2, 3, 2, 2, 1 },           //17 : [1]
        { 2, 2, 3, 2, 1, 1 },           //18 : [2]
        { 2, 2, 1, 1, 3, 2 },           //19 : [3]
        { 2, 2, 1, 2, 3, 1 },           //20 : [4]
        { 2, 1, 3, 2, 1, 2 },           //21 : [5]
        { 2, 2, 3, 1, 1, 2 },           //22 : [6]
        { 3, 1, 2, 1, 3, 1 },           //23 : [7]
        { 3, 1, 1, 2, 2, 2 },           //24 : [8]
        { 3, 2, 1, 1, 2, 2 },           //25 : [9]
        { 3, 2, 1, 2, 2, 1 },           //26 : [:]
        { 3, 1, 2, 2, 1, 2 },           //27 : [;]
        { 3, 2, 2, 1, 1, 2 },           //28 : [<]
        { 3, 2, 2, 2, 1, 1 },           //29 : [=]
        { 2, 1, 2, 1, 2, 3 },           //30 : [>]
        { 2, 1, 2, 3, 2, 1 },           //31 : [?]
        { 2, 3, 2, 1, 2, 1 },           //32 : [@]
        { 1, 1, 1, 3, 2, 3 },           //33 : [A]
        { 1, 3, 1, 1, 2, 3 },           //34 : [B]
        { 1, 3, 1, 3, 2, 1 },           //35 : [C]
        { 1, 1, 2, 3, 1, 3 },           //36 : [D]
        { 1, 3, 2, 1, 1, 3 },           //37 : [E]
        { 1, 3, 2, 3, 1, 1 },           //38 : [F]
        { 2, 1, 1, 3, 1, 3 },           //39 : [G]
        { 2, 3, 1, 1, 1, 3 },           //40 : [H]
        { 2, 3, 1, 3, 1, 1 },           //41 : [I]
        { 1, 1, 2, 1, 3, 3 },           //42 : [J]
        { 1, 1, 2, 3, 3, 1 },           //43 : [K]
        { 1, 3, 2, 1, 3, 1 },           //44 : [L]
        { 1, 1, 3, 1, 2, 3 },           //45 : [M]
        { 1, 1, 3, 3, 2, 1 },           //46 : [N]
        { 1, 3, 3, 1, 2, 1 },           //47 : [O]
        { 3, 1, 3, 1, 2, 1 },           //48 : [P]
        { 2, 1, 1, 3, 3, 1 },           //49 : [Q]
        { 2, 3, 1, 1, 3, 1 },           //50 : [R]
        { 2, 1, 3, 1, 1, 3 },           //51 : [S]
        { 2, 1, 3, 3, 1, 1 },           //52 : [T]
        { 2, 1, 3, 1, 3, 1 },           //53 : [U]
        { 3, 1, 1, 1, 2, 3 },           //54 : [V]
        { 3, 1, 1, 3, 2, 1 },           //55 : [W]
        { 3, 3, 1, 1, 2, 1 },           //56 : [X]
        { 3, 1, 2, 1, 1, 3 },           //57 : [Y]
        { 3, 1, 2, 3, 1, 1 },           //58 : [Z]
        { 3, 3, 2, 1, 1, 1 },           //59 : [[]
        { 3, 1, 4, 1, 1, 1 },           //60 : [\]
        { 2, 2, 1, 4, 1, 1 },           //61 : []]
        { 4, 3, 1, 1, 1, 1 },           //62 : [^]
        { 1, 1, 1, 2, 2, 4 },           //63 : [_]
        { 1, 1, 1, 4, 2, 2 },           //64 : [`]
        { 1, 2, 1, 1, 2, 4 },           //65 : [a]
        { 1, 2, 1, 4, 2, 1 },           //66 : [b]
        { 1, 4, 1, 1, 2, 2 },           //67 : [c]
        { 1, 4, 1, 2, 2, 1 },           //68 : [d]
        { 1, 1, 2, 2, 1, 4 },           //69 : [e]
        { 1, 1, 2, 4, 1, 2 },           //70 : [f]
        { 1, 2, 2, 1, 1, 4 },           //71 : [g]
        { 1, 2, 2, 4, 1, 1 },           //72 : [h]
        { 1, 4, 2, 1, 1, 2 },           //73 : [i]
        { 1, 4, 2, 2, 1, 1 },           //74 : [j]
        { 2, 4, 1, 2, 1, 1 },           //75 : [k]
        { 2, 2, 1, 1, 1, 4 },           //76 : [l]
        { 4, 1, 3, 1, 1, 1 },           //77 : [m]
        { 2, 4, 1, 1, 1, 2 },           //78 : [n]
        { 1, 3, 4, 1, 1, 1 },           //79 : [o]
        { 1, 1, 1, 2, 4, 2 },           //80 : [p]
        { 1, 2, 1, 1, 4, 2 },           //81 : [q]
        { 1, 2, 1, 2, 4, 1 },           //82 : [r]
        { 1, 1, 4, 2, 1, 2 },           //83 : [s]
        { 1, 2, 4, 1, 1, 2 },           //84 : [t]
        { 1, 2, 4, 2, 1, 1 },           //85 : [u]
        { 4, 1, 1, 2, 1, 2 },           //86 : [v]
        { 4, 2, 1, 1, 1, 2 },           //87 : [w]
        { 4, 2, 1, 2, 1, 1 },           //88 : [x]
        { 2, 1, 2, 1, 4, 1 },           //89 : [y]
        { 2, 1, 4, 1, 2, 1 },           //90 : [z]
        { 4, 1, 2, 1, 2, 1 },           //91 : [{]
        { 1, 1, 1, 1, 4, 3 },           //92 : [|]
        { 1, 1, 1, 3, 4, 1 },           //93 : [}]
        { 1, 3, 1, 1, 4, 1 },           //94 : [~]
        { 1, 1, 4, 1, 1, 3 },           //95 : [DEL]
        { 1, 1, 4, 3, 1, 1 },           //96 : [FNC3]
        { 4, 1, 1, 1, 1, 3 },           //97 : [FNC2]
        { 4, 1, 1, 3, 1, 1 },           //98 : [SHIFT]
        { 1, 1, 3, 1, 4, 1 },           //99 : [Cswap]
        { 1, 1, 4, 1, 3, 1 },           //100 : [Bswap]
        { 3, 1, 1, 1, 4, 1 },           //101 : [Aswap]
        { 4, 1, 1, 1, 3, 1 },           //102 : [FNC1]
        { 2, 1, 1, 4, 1, 2 },           //103 : [Astart]
        { 2, 1, 1, 2, 1, 4 },           //104 : [Bstart]
        { 2, 1, 1, 2, 3, 2 },           //105 : [Cstart]
        { 2, 3, 3, 1, 1, 1 },           //106 : [STOP]
        { 2, 1 },                       //107 : [END BAR]
    };

    enum class Jeu { None, A, B, C };

    inline char jeuStr(Jeu jeu);

    class BarcodePixmap : public QPixmap {
        public:

        private:

    };

    class BarcodePixmapTest : public QPixmap {
        public:
            BarcodePixmapTest(std::string code, int width, int height);

        private:
            QColor _bgColor;

            std::string _ABCSet;
            std::string _ASet;
            std::string _BSet;
            std::string _CSet;

            std::map<uint8_t, std::string> _SetFrom;
            std::map<uint8_t, std::string> _SetTo;
            std::map<uint8_t, uint8_t> _JStart = { { 'A', 103 }, { 'B', 104 }, { 'C', 105 } };
            std::map<uint8_t, uint8_t> _JSwap = { { 'A', 101 }, { 'B', 100 }, { 'C', 99 } };

            void drawCode(int x, int y, std::string code, int width, int height);

            void printString(std::string const& str);
            void printMapString(std::map<uint8_t, std::string> const& map);

            template <typename T, std::size_t N>
            void printArray(std::array<T, N> const& array) {
                std::stringstream sstream;
                for (int i = 0; i < array.size(); ++i) {
                    sstream << (int)(uint8_t)+array[i] << ":";
                }
                qDebug() << QString::fromStdString(sstream.str());
            }



            Jeu jeu;

    };

    class BarcodeLabel : public QLabel {
        public:
            explicit BarcodeLabel(QWidget *parent=nullptr, Qt::WindowFlags f=Qt::WindowFlags());
            explicit BarcodeLabel(const QString &text, QWidget *parent=nullptr, Qt::WindowFlags f=Qt::WindowFlags());

        protected:
            void resizeEvent(QResizeEvent * event) override;
            BarcodePixmapTest * _pixmap;
    };
}



#endif //CODE128_H
